#!/bin/bash
# Copyright (c) 2024 Telefónica Innovación Digital
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Alejandro Muñiz - Autor
#     Luis M. Contreras - Coautor


# Obtenemos la lista de nodos del clúster Kind
nodes=$(kubectl get nodes --no-headers -o custom-columns=":metadata.name")

# Definimos los comandos a ejecutar en cada nodo
commands="
apt update &&
apt install -y apache2 ufw python3 python3-pip &&
systemctl start apache2 &&
systemctl enable apache2 &&
ufw allow 80/tcp &&
ss -tuln | grep :80
"

# Iteramos sobre cada nodo y ejecutamos los comandos necesarios
for node in $nodes; do
  echo "Instalando dependencias en el nodo: $node"
  # Utilizamos kubectl para ejecutar los comandos dentro de cada nodo
  docker exec -it $node /bin/bash -c "$commands"
  #docker cp ../all-requirements.txt $node:/
  #docker exec -it $node /bin/bash -c "pip3 install -r /all-requirements.txt"
  echo "Dependencias instaladas y configuradas en el nodo: $node"
done

echo "Instalación completada en todos los nodos."
