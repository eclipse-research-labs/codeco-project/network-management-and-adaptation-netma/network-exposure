#!/bin/bash
# Copyright (c) 2024 Telefónica Innovación Digital
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Alejandro Muñiz - Autor
#     Luis M. Contreras - Coautor

# Variables
REPO_URL="https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-state-management.git"
REPO_URL2="https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-exposure.git"
REPO_DIR="network-state-management/netma-nsm-mon/k8s-netperf"
K8S_CONFIG="k8s-netperf.yaml"
PYTHON_SCRIPT="Monitoring/network_probe.py"
# Encontrar el contenedor del nodo control-plane
NODE=$(docker ps -q --filter "name=kind-control-plane")

# Comprobar si se encontró el nodo control-plane
if [ -z "$NODE" ]; then
    echo "No se encontró el nodo control-plane."
    exit 1
fi

echo "Accediendo al nodo: $NODE"

# Clonar el repositorio y ejecutar comandos en el contenedor del nodo
docker exec -it "$NODE" bash -c "
    # Instalar git y python3 si no están instalados
    if ! command -v git &> /dev/null; then
        apt-get update && apt-get install -y git
    fi
    apt-get install -y python3 python3-pip tcpdump

    # Clonar el repositorio
    git clone $REPO_URL

    # Navegar al directorio del repositorio
    cd $REPO_DIR

    # Aplicar la configuración de Kubernetes
    kubectl taint nodes kind-control-plane node-role.kubernetes.io/control-plane-
    kubectl apply -f $K8S_CONFIG
    cd ..
    sleep 60
    # Ejecutar el script de Python
    python3 $PYTHON_SCRIPT &

    # Pasamos al Nemesys
    git clone $REPO_URL2
    cd network-exposure
    pip3 install -r requirements.txt
    python3 nemesys.py --delay 60 --l2sm 'prometheus-lpm-network:9090' &
    sleep 20
"

echo "Proceso completado en el nodo: $NODE"

