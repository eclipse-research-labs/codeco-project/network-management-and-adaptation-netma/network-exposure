<!--
  ~ Copyright (c) 2023 Telefónica Innovación Digital
  ~ 
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~ 
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~ 
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  ~ 
  ~ SPDX-License-Identifier: Apache-2.0
  ~ 
  ~ Contributors:
  ~     Alejandro Muñiz - Autor
  ~     Luis M. Contreras - Coautor
  ~
-->

## Table of contents
1. [General description](#general-description)
2. [List of files](#list-of-files)
3. [Ecosystem Preparation](#ecosystem-preparation)
4. [Functional deployment](#functional-deployment)
5. [Non functional improves](#non-functional-improves)
6. [Versions](#versions)
7. [Authors and acknowledgment](#authors-and-acknowledgment)
8. [License](#license)
9. [Project status](#project-status)
10. [ToDo list](#todo-list)
11. [How to colaborate](#how-to-colaborate)

## General description
Directory created to include the code related with the NetMA Exposure System Component. This component will be based on Kubernetes native tools and an ALTO implementation. As it is described in D12, Network Exposure System is the component that will work as a North-bound interface (NBI) for NetMA. It will interact with both PDLC and ACM to send them the network performance metrics for both underlay and overlay topology. Also, in the multi-cluster scenario, it could also include the interactions to exposure to SWM, but it would be a further modification to discuss if the scenario requires it.

The Network Exposure System (Nemesys) has two main functionalities: On the one side it will be the component that works as a topology view integrator, receiving information from multiple sources and integrating it in a single picture. On the other side, it is the subcomponent in charge of creating the NetMA CRD and to send the performance information to ACM via Prometheus. 

To be able to realize this assignment, Nemesys will use a python engine to integrate Overlay and Underlay view, creating matches in Compute nodes  (both in underlay and overlay) and end-to-end links (also, between both networks). 
 
The first step followed by Nemesys sub-component once it is deployed will be to stablish connection with Prometheus pod. As NetMA will be launched all together, each IP:Port peer can be defined static, being a parameter to include in configuration files. These files will be, at least for the first NetMA version, unique for each sub-component, but in a v2 where all functionalities and dependencies are clear and static, it could be centralized in a single document that configures each of the sub-components. This “small” change could bring a deeper integration, reducing flexibility but increasing the interoperability.
Once connection with the NetMA Prometheus is stablished, it will start reading metrics, mapping the entries’ names obtained from Prometheus with the metrics to expose. These values will be integrated into two views (overlay topology, underlay compute domain) that will be related according with the Kubernetes node name. As both Underlay and Overlay monitoring tools use K8S-based IDs, names will match.
	
This process will be realized periodically each X seconds (need to be defined by the user; default time is 300s), and once the values are updated, they will be sent to the functions that push the metrics to ACM’s Prometheus and that creates a new NetMA Custom Resource.
		 
Finally, the requirements defined for this module are the following ones:
+ Device capabilities: no CPU or RAM dependencies. At least one network card with Internet connection.
+ Software dependencies: Kubernetes installed (no version dependencies). The image created with the code has other dependencies, but, as the code will be deployed into a docker image, those dependencies will not be needed in the host device.
+ Configuration dependencies: There are 5 parameters to be defined before launching this module:
	+ --acm: URL where we are pushing the metrics to ACM. Example: “http://1.2.3.4:9090/api/”
	+ --l2sm: URL where the overlays metrics are being read from. To be integrated under --prometheus parameter. Example: “92.168.0.1:8080”
	+ --probes: URL where the underlays metrics are being read from. To be integrated under --prometheus parameter. Example: “92.168.0.1:9090”
	+ --prometheus: URL where the internal DB is launched. In the moment of the code release this parameter is split into --acm and --probes, expecting to finalise this API integration before end of September 2024. Example: “1.2.3.4:9090/”
	+ --metrics: List of metrics used. In Prometheus there could be more metrics to be used internally, therefore we should count with the list of metrics to use once they are read. Example: ["net_jitter_ms", "net_rtt_ms", "net_throughput_kbps"]
	+ --alto-api: API where we want ALTO to be exposed. In version 1, it will not be included. Example: [“1.2.3.4”, “8080”]
	+ --delay: Seconds between CR updates. Example: 300


### List of files

+ alto/: folder with ALTO's code. It will be used to integrate routing devices' information. Not used in v1.
+ kuberfiles/: folder with the resources to be deployed:
	+ 00_namespace.yaml: definition of he-codeco-netma namespace.
	+ 01_netma-topology-crd.yaml: NetMA's CRD file.
	+ 02_nemesys-deployment.yaml: Network Exposition System's K8S version.
+ tests/: list of examples for internal request. Usefull for testing.
+ Dockerfile: Definition to dockerize Nemesys module.
+ nemesys.py: Core code for this component.
+ netma-topology-metrics.yaml: Example of the current CR.
+ requirements.txt: Document with Python dependencies.

### Ecosystem preparation and nemesys deployment

Before deploy the Nemesys sub-component, it is needed to deploy the overlay and underlay monitoring sub-components available in the NetMA component folders "Network State Management" and "Secure Connectivity".

Then, it is needed to clone the Nemesys repository:
```
git clone https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-exposure.git
```
Or at least download the three files in the kuberfiles folder.

Once the Ecosystem is launched. We can deploy the resources:

```
kubectl apply -f kuberfiles/00_namespace.yaml
kubectl apply -f kuberfiles/01_netma-topology-crd.yaml
kubectl apply -f kuberfiles/02_nemesys-deployment.yaml
```

### How to Deploy NetMA

First, it is needed to have a k8s cluster deployed with the next requirements:
```
# First we clone the required folders...
git clone "https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/secure-connectivity.git" "secure-connectivity"
git clone "https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-exposure.git" "network-exposure"
git clone "https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-state-management" "network-state-management"

# ... And dependencies, applying them
git clone https://github.com/containernetworking/plugins /tmp/plugins
/tmp/plugins/build_linux.sh
sleep 20
kubectl apply -f https://github.com/flannel-io/flannel/releases/latest/download/kube-flannel.yml 
sleep 20
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.3/cert-manager.yaml
kubectl apply -f https://raw.githubusercontent.com/k8snetworkplumbingwg/multus-cni/master/deployments/multus-daemonset-thick.yml
kubectl taint nodes kind-control-plane node-role.kubernetes.io/control-plane:NoSchedule-
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus-operator prometheus-community/kube-prometheus-stack -n he-codeco-netma
sleep 60

# Once the requirements are fulfilled, there is needed to deploy the k8s customed resources
cd network-exposure/
kubectl apply -f kuberfiles/00_namespace.yaml
kubectl apply -f kuberfiles/01_netma-topology-crd.yaml
cd ..

# Installing the L2SM component
cd secure-connectivity/
kubectl create -f ./deployments/l2sm-deployment.yaml -n=he-codeco-netma
cd ..

# Installing the NSM component
# If the NSM probe used is the npp:
chmod 755 network-exposure/npp-script.sh
./network-exposure/npp-script.sh
kubectl apply -f network-state-management/netma-nsm-npp/daemonset.yaml

# else, if the NSM probes used is the mon
cd network-state-management/netma-nsm-npp/k8s-netperf
kubectl apply -f k8s-netperf.yaml
cd ..

# Finally, it is needed to install the Nemesys sub-component
cd network-exposure/
kubectl apply -f kuberfiles/02_nemesys-deployment.yaml

```


### Non Functional improves

To be evaluated during the next cycle of features.

### Versions

v1
This is the original version. It includes the basic functionality for NetMA, what includes the recopilation of the minimal Overlay + Underlay metrics set. 

v2
First early-stage version for single-cluster domains. Include the functionalities defined in D12 and explained before.

### Authors and acknowledgment
For any doubt, please contact Alejandro Muñiz Da Costa: alejandro.muniz@telefonica.com

### License
© 2024 Telefónica Innovación Digital

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

### Project status

Current on an early deployment state.


## ToDo List

Here there will be listed the current Open Issues defined in D12 related to this component:

+ NetMA-OI-1: Close the internal integration in a unified way. Currently each component has one interface (Prometheus, API/rest, …) to expose the metrics but the idea is to integrate all the metrics in a unified way. Internal integration.
+ NETMA-OI-2: Once we count with the final beta version, it will be needed to check the escalation and automation of NetMA. Non-Functional Improvements.
+ NETMA-OI-3: Worst case scenario. We are currently testing the main workflow, but we need also to define how to act if there are complications or failures, either internal or external. Non-Functional Improvements.


## How to colaborate

***
- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-exposure.git
git branch -M main
git push -uf origin main
```

### Integrate with your tools
- [ ] [Set up project integrations](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/network-management-and-adaptation-netma/network-exposure/-/settings/integrations)

### Test and Deploy
Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


