# Copyright (c) 2024 Telefónica Innovación Digital
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Alejandro Muñiz - Autor
#     Luis M. Contreras - Coautor

from time import sleep
import json
# import yaml
import requests
from datetime import datetime
from kubernetes import config, client
# from prometheus.Writer import Cwriter
import sys
import subprocess

TEST_MODE = 0

class Nemesys:

    def __init__(self, metrics, update_delay, probes_s="netperf"):
    #def __init__(self, metrics, alto_demon, update_delay, acm_uri, l2sm="", probes="", probes_s="netperf"):
        '''
            Class used to proccess the different sources of information in NetMA.
            Imput:
                Metrics: List of collected metrics and how are they mapped in the CRD.
                update_delay: expected periodicity between updates.
                acm_uri: URI where metrics will be pushed.
                ip and port: Peer used to access to internal prometheus where metrics will be colllected.
            Paramethers:
                self.url.pmth = url where the internal prometheus is launched.
                self.metrics = param with the metrics received.
                self.cr = last active CR instance. It starts at null.
                self.alto = ALTO demon that compilates network metrics.
                self.acm_uri = uri where metrics will be pushed.
        '''
        config.load_incluster_config()
        self.k8s = client.CoreV1Api()
        self.k8s_client = client.ApiClient()
        self.custom_api = client.CustomObjectsApi(self.k8s_client)
        self.probes = []
        if probes_s != "netperf":
            pods = self.k8s.list_namespaced_pod(namespace='he-codeco-netma', label_selector="app=network-probe")
            for pod in pods.items:
                url = "http://"+str(pod.status.pod_ip)+":5001"
                self.probes.append(url)
        l2sm_p =  self.k8s.list_namespaced_pod(namespace='he-codeco-netma', label_selector="app=prometheus-lpm-network")
        print("L2SM Pods:\n", l2sm_p.items)
        if len(l2sm_p.items):
            for l2sm in l2sm_p.items:
                self.url_pmth = "http://"+ str(l2sm.status.pod_ip) + ":9090"
        else:
            self.url_pmth = ""
        self.probes_src = probes_s
        self.metrics = []
        self.delay = int(update_delay)
        self.cr={}
        #self.alto = alto_demon
        #self.acm_uri = acm_uri
        for item in metrics:
            self.metrics.append(item)



    ###########################
    # Metric digest functions #
    ###########################

    def get_metric_list(self):
        '''
            Recopilation of the metrics collected in internal prometheus.
            Output:
                List of metrics available in prometheus.
        '''
        prometheus_url = self.url_pmth  + '/api/v1/label/__name__/values'
        # prometheus_url = 'http://10.96.181.243:9090/api/v1/label/__name__/values'
        if not TEST_MODE:
            try:
                response = requests.get(prometheus_url, timeout=2)
                data = response.json()
            except requests.exceptions.RequestException:
                return {}
        else:
            response = self.read_test_sources("get_metric_list_format")
            data = json.loads(response.replace("'",'"'))
        metrics = []
        # print("GET_METRIC_LIST:\t", data)
        # datos = data["data"]
        for metrica in data["data"]:
            if metrica[0:3] == "net":
                metrics.append(metrica)
        #print("Metricas:\t", metrics)
        return metrics


    def get_metric_value(self, metric_name):
        '''
            Request metrics to the prometheus database.
            Input:
                metric_name: name of the metric to be lokked for. It follows the format name_hash.
            Output:
                Information extracted from prometheus.
        '''
        prometheus_url = self.url_pmth  + '/api/v1/query'
        # prometheus_url = 'http://10.96.181.243:9090' + '/api/v1/query'
        query = f'query={metric_name}'
        if not TEST_MODE:
            response = requests.get(f'{prometheus_url}?{query}')
            data = response.json()
        else:
            resp = self.read_test_sources("get_metric_value_format")
            data = json.loads(resp.replace("'",'"'))[metric_name]
        # print("get_metric_value:\t", data)
        result = {}
        result['value'] = data['data']['result'][0]['value'][1]
        metric = metric_name.split("_")[:-1]
        result["metric"] = "_".join(metric)
        result["src"] = data['data']['result'][0]['metric']['source_node']
        result["dst"] = data['data']['result'][0]['metric']['target_node']
        # print("METRICA:\t", result)
        return result


    def guardar_metricas_json(metricas, archivo_salida):
        '''
            Saves the metrics in a output file. If the file is not available (no permissions, wrong name, ...) it does nothing.
            Imput:
                metricas: list of metrics received. Expected in a JSON format.
                archivo_salida: file where metrics will be saved. 
        '''
        if not TEST_MODE:
            with open(archivo_salida, 'w') as f:
                json.dump(metricas, f, indent=4)
        else:
            print("Metrics saved in json:\t", metricas)

    def get_underlay_metrics(self, underlay_url="http://10.103.9.188:8090/metrics"):
        '''
            This function extracts the metrics from underlay metrics API.
            It is expected to remove/update this method in future versions once we have a final datamodel for underlay metrics.
            Imput:
                underlay_url: url to be read.
            Output:
                Raw data from underlay monitoring tools.
        '''
        if not TEST_MODE:
            resp = requests.get(underlay_url)
            response = str(resp.text)
        else:
            response = self.read_test_sources("get_underlay_metrics_format")
        data = []
        for req in response.split('\n'):
            if len(req) < 1:
                continue
            if req[0] != "#":
                data.append(req)
        # print("UNDERLAY DATA:\n",data)
        # print("get_underlay_metrics:\t", data)
        return data


    def process_prometheus_umetrics(self, data):
        '''
            Proccess the prometheus underlay metrics.
            To be updated once all metrics are integrated in a common format.
            Imput:
                data: raw information obtained from network state monitoring module.
            Output:
                nodes and links dictionaries with its respective metrics.
        '''
        metrics = {"network_metric_latency":"uLatencyNanos","network_metric_throughput":"uBandWidthBits", "network_metric_packet_loss":"uPacketLoss", "network_metric_link_energy":"uLinkEnergy", "network_metric_link_failure":"uLinkFailure"}
        datos = {}
        nodos = {}
        for metrica in data:
            ident,valor = metrica.split(" ")
            if "{" not in ident:
                #print("Nos saltamos la iteracion:\t", metrica)
                continue
            met,pseudo_json = ident.split("{")
            if met not in metrics.keys():
                #print("Nos saltamos la iteracion:\t", metrica)
                continue
            jason = '{"'+pseudo_json.replace("=",'":').replace(',',',"')
            #print("JASON:\t",jason)
            labels = json.loads(jason)
            #src = pseudo_json.split(",")[0].split("=")[1][1:-1]
            src = labels["current_node"]
            #dst = pseudo_json.split(",")[1].split("=")[1][1:-2]
            dst = labels["remote_node"]

            link = "link_" + src + "_" + dst
            if src not in nodos.keys():
                nodos[src] = {"uNodeDegree": 0, "uNodeBandWidth":"0"}
            if dst not in nodos.keys():
                nodos[dst] = {"uNodeDegree": 0, "uNodeBandWidth":"0"}

            if link not in datos:
                datos[link] = {}
                nodos[src]["uNodeDegree"] = nodos[src]["uNodeDegree"] + 1
                nodos[dst]["uNodeDegree"] = nodos[dst]["uNodeDegree"] + 1
            #param = "".join(str(x+"_") for x in met.split("_")[:-1])
            if met in metrics.keys():
                datos[link][metrics[met]] = str(valor)

            if met == "network_metric_throughput":
                nodos[src]["uNodeBandWidth"] = str(float(nodos[src]["uNodeBandWidth"]) + float(valor))
        # print("DATOS:\n", datos)
        return nodos, datos


    def process_overlay_topo(self,metricas):
        '''
            Proccess the prometheus overlay metrics.
            Imput:
                metricas: List of available metrics in the overlay topology prometheus.
            Output:
                nodes and links dictionaries with its respective metrics.
        '''
        nodos = {}
        links = []
        metrics = {}
        met_n = {"net_rtt_ms":"oLatencyNanos","net_throughput_kbps":"oBandWidthBits"}
        for met in metricas:
            valor = self.get_metric_value(met)
            if valor["metric"] in met_n.keys():
                if valor['src'] not in nodos.keys():
                    nodos[valor['src']]={"oNodeDegree":0, "oNodeThroughput":0}
                if valor['dst'] not in nodos:
                    nodos[valor['dst']]={"oNodeDegree":0, "oNodeThroughput":0}
                if (valor['src'],valor['dst']) not in links:
                    links.append((valor['src'],valor['dst']))
                    nodos[valor['dst']]["oNodeDegree"] = nodos[valor['dst']]["oNodeDegree"]+0.5
                    nodos[valor['src']]["oNodeDegree"] = nodos[valor['src']]["oNodeDegree"]+0.5
                if (valor['src']+"_"+valor['dst']) not in metrics.keys():
                    metrics[valor['src']+"_"+valor['dst']]={}
                    metrics[valor['src']+"_"+valor['dst']][met_n[valor["metric"]]]=str(valor['value'])
                else:
                    metrics[valor['src']+"_"+valor['dst']][met_n[valor["metric"]]]=str(valor['value'])
                if valor["metric"] == 'net_throughput_kbps':
                    nodos[valor['src']]["oNodeThroughput"]= nodos[valor['src']]["oNodeThroughput"] + int(valor['value'])/1e6
                elif valor["metric"] == 'net_rtt_ms':
                    metrics[valor['src']+"_"+valor['dst']][met_n[valor["metric"]]] = str(metrics[valor['src']+"_"+valor['dst']][met_n[valor["metric"]]])+"e6"
        return nodos,metrics



    ###############################
    # Exposure-oriented functions #
    ###############################

    def crear_custom_resource(self, nodos, metrics):
        '''
            From a dictionary of nodes and metrics obtains the information to feed the NetMA Custom Resource.
            Imput: 
                nodos: dictionary of nodos. It follows the format later indicated.
                metrics: dictionary with the links and netrics associated. It follows the format later indicated.
            
                Both imputs follows the next data format: {"over":{},"under":{"compute":{},"net":{}}} 
                    Over is the dictionary where overlay is launched; underlay/compute is the dictionary where E2E underlay metrics are stored; and underlay/net is the equivalent for routing devices.
                    Imput is received from other Nemesys function, therefore imput should always follow the same datamodel, no mather the source used.

            Output:
                Custom Resource with the format devined in netma-topologies.codeco.com CRD:
                    If overlay is not provided, it do not include information in such section. 
                    If underlay/compute is not provided but we have the overlay, this info is compketed with overlay metrics.
                    If neither overlay or underlay/compute are provided, both sections will be empty.
                    If underlay/net is not provided, it do not include information in such section.
        '''
        
        onodes = []
        oenlaces = []
        unodes = []
        uenlaces = []
        upaths = []

        # Creation of the baseline
        json_cr ={"apiVersion": "codeco.com/v1","kind": "NetmaTopology","metadata":{"name":"netma-sample","namespace": "he-codeco-netma"}}
        json_cr["overlay-topology"] = {}
        json_cr["underlay-topology"] = {}

        # Overlay Metrics
        for nodo in nodos["over"].keys():
            nodos["over"][nodo]["oNodeDegree"] = int(nodos["over"][nodo]["oNodeDegree"])
            nodos["over"][nodo]["oNodeThroughput"] = str(nodos["over"][nodo]["oNodeThroughput"])
            onodes.append({"name":nodo,"type":"COMPUTE", "capabilities":nodos["over"][nodo]})
        for enlace in metrics["over"].keys():
            metrics["over"][enlace]["oLatencyNanos"] = int(float(metrics["over"][enlace]["oLatencyNanos"]))
            oenlaces.append({"name":enlace,"source":str(enlace.split("_")[0]), "target":str(enlace.split("_")[1]), "capabilities":metrics["over"][enlace]})
           
        json_cr["overlay-topology"] = {"networkImplementation":"l2sm-network", "physicalBase":"logical-network", "nodes":onodes, "links":oenlaces}
        
        # Underlay Metrics (Compute and Network)
        
        # If underlay is empty: copy the overlay
        if not nodos["under"]["compute"]:
            for nodo in onodes:
                unodes.append({"name":nodo["name"],"type":"COMPUTE", "capabilities":{"uNodeDegree": int(int(nodo["capabilities"]["oNodeDegree"])/2), "uNodeBandWidth": str(nodo["capabilities"]["oNodeThroughput"])}})
            for enlace in oenlaces:
                upaths.append({"name":enlace["name"],"source":enlace["source"], "target":enlace["target"], "capabilities":{"uPathFailure":0,"uPathLength":1,"uPacketLoss":0,"uBandWidthBits":str(enlace["capabilities"]["oBandWidthBits"]), "uLatencyNanos":str(enlace["capabilities"]["oLatencyNanos"])}})
        else:
            for nodo in nodos["under"]["compute"].keys():
                unodes.append({"name":nodo,"type":"COMPUTE", "capabilities":{"uNodeDegree": int(int(nodos["under"]["compute"][nodo]["uNodeDegree"])/2), "uNodeBandWidth":str(nodos["under"]["compute"][nodo]["uNodeBandWidth"])}})
            for enlace in metrics["under"]["compute"].keys():
                upaths.append({"name":enlace,"source":str(enlace.split("_")[1]), "target":str(enlace.split("_")[2]), "capabilities":{"uPathFailure": int(float(metrics["under"]["compute"][enlace]["uLinkFailure"])),"uPathLength":1,"uPacketLoss":int(float(metrics["under"]["compute"][enlace]["uPacketLoss"])),"uBandWidthBits":str(metrics["under"]["compute"][enlace]["uBandWidthBits"]), "uLatencyNanos":str(metrics["under"]["compute"][enlace]["uLatencyNanos"])}})
                link = enlace.split("_")[1] + "_" + enlace.split("_")[2]
                uenlaces.append({"name":link,"source":str(enlace.split("_")[1]), "target":str(enlace.split("_")[2]),"capabilities":{"uLinkFailure":int(float(metrics["under"]["compute"][enlace]["uLinkFailure"])),"uLinkEnergy":int(float(metrics["under"]["compute"][enlace]["uLinkEnergy"]))}})

        if nodos["under"]["net"]:
            pass

        json_cr["underlay-topology"] = {"networkImplementation":"underlay-network", "physicalBase":"physical-network", "nodes":unodes, "links":uenlaces, "paths":upaths}
        # print("JSON CR:\t", json_cr) 

        # print("NODES:\t", nodes)
        # print("ENLACES:\t", enlaces)
        # cr = yaml.dump(json_cr, sort_keys=False, allow_unicode=True)
        
        if TEST_MODE:
            print("Custom Resource created:\n", json_cr)
        
        return json_cr


    def guardar_cr(self, cr, nombre="metrics.yaml" ):
        '''
            It stores the CR created in a YAML file.
            Imput:
                cr: custom resource created.
                nombre: name of the doc created.
            Output:
                YAML file saved in the same folder where the code is runing.
        '''
        # Guardar el YAML en un archivo
        with open(nombre, 'w') as yaml_file:
            yaml_file.write(cr)

    def edit_metrics_to_prometheus(self, nodes, metrics):
        '''
            This function proccess the data storage in local to define a datamodel readable by Prometheus.
            Inputs:
                content: Standard imput to be proccess.
            Output:
                List with same imput but formated.
        '''
        prometheus_metrics = []
        # Process "over" metrics
        for link, metrics_dict in metrics["over"].items():
            source, target = link.split("_")
            for metric_name, value in metrics_dict.items():
                prometheus_metrics.append({
                    'metric_name': metric_name,
                    'labels': {'source': source, 'target': target, 'topology': 'NetworkOverlay'},
                    'value': value
                })


        # Process "over" nodes
        for node, metrics_dict in nodes["over"].items():
            for metric_name, value in metrics_dict.items():
                prometheus_metrics.append({
                    'metric_name': metric_name,
                    'labels': {'node' : node, 'topology': 'NetworkOverlay'},
                    'value': value
                })


        # Process "under" metrics
        for link, metrics_dict in metrics["under"]["compute"].items():
            link_name = link.replace("link_", "")
            source, target = link_name.split("_")
            for metric_name, value in metrics_dict.items():
                prometheus_metrics.append({
                    'metric_name': metric_name,
                    'labels': {'source': source, 'target': target, 'topology': 'NetworkUnderlay'},
                    'value': value
                })

        # Process "under" nodes
        for node, metrics_dict in nodes["under"]["compute"].items():
            for metric_name, value in metrics_dict.items():
                prometheus_metrics.append({
                    'metric_name': metric_name,
                    'labels': {'node' : node, 'topology': 'NetworkOverlay'},
                    'value': value
                })

        # print(prometheus_metrics)
        return prometheus_metrics

    def get_configmap_yaml(configmap_name, namespace="default"):
        try:
            # Ejecuta el comando kubectl para obtener el ConfigMap en formato YAML
            configmap = self.k8s.read_namespaced_config_map(name="netperf-metrics", namespace="he-codeco-netma")
            # result = subprocess.run(["kubectl", "get", "configmap", "netperf-metrics", "-o", "yaml"], capture_output=True,text=True)
            # Parsear la salida YAML a un diccionario de Python
            # configmap = yaml.safe_load(result.stdout)
            
            return configmap
        
        except subprocess.CalledProcessError as e:
            print(f"Error al ejecutar kubectl: {e}")
            return None

    def process_configmap_metrics(self):
        configmap = self.get_configmap_yaml("netperf-metrics")
        result_dict = {}
        nodes = {}
        metrics = {"bw":"uNodeBandWidth","loss":"uPacketLoss","energy":"uLinkEnergy","failure":"uLinkFailure","latency":"uLatencyNanos"}
        if configmap:
            # Cargar los datos YAML
            #print(configmap)
            #configmap_data = yaml.safe_load(configmap)
            configmap_data = configmap
            result_dict = {}
            # Iterar sobre las claves de los datos
            for key, value in configmap_data["data"].items():
                # Dividir la clave en componentes
                components = key.split(".")
                # Inicializar un puntero a la posición actual en result_dict
                origin = ""
                destin = ""
                metric = ""
                nv = 0
                # Iterar sobre los componentes para construir el diccionario anidado
                for index in (range(len(components)-1)):
                    #print("COMPONENT:\t", components[index])

                    if (components[index] == "loss") or (components[index] == "energy") or(components[index] == "failure") or(components[index] == "latency"):
                        metric = metrics[components[index]]
                        #print("Metric:\t", metric)
                    if (components[index] == "origin") or (components[index] == "from"):
                        origin = components[index+1]
                        #print("Origen:\t", origin)
                        if origin not in nodes.keys():
                            nodes[origin] = {"uNodeDegree":1, "uNodeBandWidth":nv}
                    if (components[index] == "destination") or (components[index] == "to"):
                        destin = components[index+1]
                        if destin not in nodes.keys():
                            nodes[destin] = {"uNodeDegree":1, "uNodeBandWidth":0}
                        #result_dict[origin][components[index+1]][metric]=""
                        #print("Destination:\t", components[index+1])
                    if (components[index] == "bw"):
                        metric = "uBandWidthBits"
                        nv = value.split(" ")[0]
                        if origin in nodes.keys():
                            nodes[origin]["uNodeBandWidth"] = nv
                if (origin == "") or (destin == "") or (metric == "") :
                    continue
                link_name = "link_" + origin + "_" + destin
                if link_name not in result_dict.keys():
                    result_dict[link_name] = {"source":origin, "target":destin}

                # Extraer el valor y convertirlo al tipo apropiado
                #print("VALUE:\t", value)
                if value.isnumeric():
                    result_dict[link_name][metric] = float(value)
                elif value.endswith("%"):
                    result_dict[link_name][metric] = float(value[:-1]) / 100
                else:
                    number = value.split(".")
                    while (len(number)>1) and not number[-1].isnumeric():
                        number = number[:-1]
                    if len(number) == 1:
                        result_dict[link_name][metric] = int(number[0])
                    else:
                        result_dict[link_name][metric] = float(str(number[0]+"."+number[1]))
        # Imprimir el diccionario transformado
        # print("Diccionario ConfigMap:\n", result_dict)
        return nodes, result_dict

    def send_metrics_acm(self, content, url="http://127.0.0.1:9090/api/v1/write"):
        '''
            It pushes the metrics into ACM's prometheus.
            Imput:
                content: CR with the metrics to be pushed.
                url: ACM prometheus writing API.
            Output:
                Prometheus metrics stored. No inner output.
        '''
        # url = "http://10.244.2.38:9090/api/v1/write"
        w_prom = Cwriter(url)
        lines = []
        if not TEST_MODE:
            for metric_data in content:
                w_prom.write(metric_data['metric_name'], metric_data['labels'], float(metric_data['value']))
        else:
            for metric_data in content:
                print("Metric sent to ACM:\t", metric_data)

    ### Testing function
    def read_test_sources(self, funt):
        name = "tests/" + str(funt)
        with open(name, "r") as file:
            try:
                content = file.read()
            except:
                print("TEST FILES NOT FOUND.\n")
        return content

    def apply_custom_resource(self, cr):
        try:
            # Try to replace the custom resource if it already exists
            group="codeco.com"
            version="v1"
            namespace="he-codeco-netma"
            plural="netma-topologies"
            name="netma-sample"
            try:
                resource = self.custom_api.get_namespaced_custom_object(group, version, namespace, plural, name)
                cr["metadata"]["resourceVersion"] = resource["metadata"]["resourceVersion"]
                response = self.custom_api.replace_namespaced_custom_object(group, version, namespace, plural, name, body=cr)
                print(f"Updated custom resource {name} in namespace {namespace}")
            # If the resource doesn't exist, create it
            except client.ApiException as e:
                if e.status == 404:  # Not Found
                    response = self.custom_api.create_namespaced_custom_object(group=group, version=version, namespace=namespace, plural=plural, body=cr)
                    print(f"Created new custom resource {name} in namespace {namespace}")
                else:
                    raise
        except client.ApiException as e:
            print(f"Failed to apply custom resource: {e}")
        except Exception as e:
            print(f"An error occurred: {e}")


    #############################################################
    # Main functionalities functions / functions to be executed #
    #############################################################

    def manage_topology_updates(self):
        '''
            Main function in Nemesys. It reads the information from all available sources and integrates them all in a single CR that later it is expossed.
            To be updated in version 2 to accept custom monitoring requests.
        '''
        #if (1 == 1):
        test  = 0
        while not test:
            # Reloading lists.
            nodos = {"over":{},"under":{"compute":{},"net":{}}}
            links = []
            metrics = {"over":{},"under":{"compute":{},"net":{}}}
           
            # Metricas overlay
            print("L2SM Prometheus:\t", self.url_pmth)
            if self.url_pmth == "":
                l2sm_p =  self.k8s.list_namespaced_pod(namespace='he-codeco-netma', label_selector="prometheus-lpm-network")
                if len(l2sm_p.items):
                    for l2sm in l2sm_p.items:
                        self.url_pmth = "http://"+ str(l2sm.status-pod_ip) + ":9090"
            metricas = self.get_metric_list()
            print("metricas:\t", metricas)
            if len(metricas) > 0 :
                nodos["over"],metrics["over"] = self.process_overlay_topo(metricas)
                print("Overlay metrics loaded OK\n")
            else:
                print("Overlay not detected.")
                self.url_pmth = ""

            
            ## Metricas compute underlay
            mets = {}
            met = []
            if self.probes_src == "netperf":
                mets = self.process_configmap_metrics()
            elif self.probes_src == "tcp-probes":
                if TEST_MODE:
                    met = eval(self.read_test_sources("get_underlay_metrics_format"))
                else:
                    for uri in self.probes:
                        met.extend(self.get_underlay_metrics(uri))
                        # print("MET:\t", met)
            mets = self.process_prometheus_umetrics(met)
            nodos["under"]["compute"],metrics["under"]["compute"] = mets

            print("\nUnderlay metrics loaded OK:")
            # print("NODOS:\t", nodos["under"]["compute"])
            # print("METRICAS:\t", metrics["under"]["compute"])

            ## Metricas network underlay
            ## Cargamos desde ALTO.

            ###### PENDING


            #print("NODOS:\t", nodos)
            #print("METRICAS:\t", metrics)

            #if not self.cr:
            cr = self.crear_custom_resource(nodos, metrics)
            print("\nAplicando\n")
            self. apply_custom_resource(cr)
            #else:
            #    cr = self.actualizar_cr(nodos,metrics)
            
            
            # self.guardar_cr(cr, 'netma-topology-metrics.yaml')
            # utils.create_from_yaml(self.k8s_client, 'netma-topology-metrics.yaml')
            #subprocess.run(['kubectl', 'apply', '-f', 'netma-topology-metrics.yaml'])
            #if not TEST_MODE:
            #    # Function to push metrics to ACM prometheus
            #    metrics_formated = self.edit_metrics_to_prometheus(nodos, metrics)
            #    #self.send_metrics_acm(metrics_formated, self.acm_uri)




            #print("Topology loaded")
            #data = '{"pids":'+ '""' +',"nodes-list": '+ str(list(nodos.keys())) +',"costs-list": '+ str(metrics) +',"prefixes": '+ '""' +"}"
            #data = data.replace("'", '"')
            #print(data)
            #self.return_info(3,0,1, data)
            sleep(self.delay)

            if TEST_MODE:
                test = 1


if __name__ == "__main__":

    ##################
    # Execution code #
    ##################

    # URI where the ACM's prometheus reads the metrics.
    acm_url = "http://127.0.0.1:9090/api/v1/write"
    # Peer IP:Port where the internal Prometheus is launched.
    l2sm = "10.96.181.243:9090"
    probes = ["http://10.244.0.163:5001/","http://10.244.2.80:5001/","http://10.244.1.153:5001/"]
    # Name of the metrics stored in the local Prometheus. 
    metricas2 = ["net_jitter_ms","net_rtt_ms","net_throughput_kbps"]
    # Expected delay between updates
    delay = 10
    

    args = sys.argv[1:]  # Excluimos el nombre del script
    i = 0

    if len(args) and args[0] == "-t":
        TEST_MODE = 1

    else:
        while i < len(args):
            arg = args[i]
            if arg == '--delay':
                delay = args[i + 1]
                i += 2
            elif arg == '--acm':
                acm_url = args[i + 1]
                i += 2
            elif arg == '--l2sm':
                l2sm = args[i + 1]
                i += 2
            elif arg == '--probes':
                probes = args[i + 1]
                i += 2
            else:
                i += 1



    nemesys = Nemesys(metricas2, delay, probes)
    nemesys.manage_topology_updates()


    # Execution should not pass this step.

    ##############
    # Raw tests. #
    ############## 

    # Métricas que deseas recuperar
    metricas = [
        "node_name",
        "link_id",
        "link_failure",
        "packet_loss",
        "node_net_failure",
        "path_failure",
        "node_throughput",
        "ebw",
        "ibw",
        "zone",
        "node_degree",
        "latency",
        "path_length",
        "link_energy"
    ]
    values = [{'value': '0.311', 'metric': 'net_jitter_ms', 'src': 'netma-test-2', 'dst': 'netma-test-3'}, {'value': '2.003', 'metric': 'net_jitter_ms', 'src': 'netma-test-2', 'dst': 'netma-test-1'}, {'value': '0.097', 'metric': 'net_jitter_ms', 'src': 'netma-test-3', 'dst': 'netma-test-2'}, {'value': '0.397', 'metric': 'net_jitter_ms', 'src': 'netma-test-3', 'dst': 'netma-test-1'}, {'value': '0.201', 'metric': 'net_jitter_ms', 'src': 'netma-test-1', 'dst': 'netma-test-2'}, {'value': '0.223', 'metric': 'net_jitter_ms', 'src': 'netma-test-1', 'dst': 'netma-test-3'},{'value': '1.411', 'metric': 'net_rtt_ms', 'src': 'netma-test-2', 'dst': 'netma-test-3'}, {'value': '1.343', 'metric': 'net_rtt_ms', 'src': 'netma-test-2', 'dst': 'netma-test-1'}, {'value': '1.067', 'metric': 'net_rtt_ms', 'src': 'netma-test-3', 'dst': 'netma-test-2'}, {'value': '1.145', 'metric': 'net_rtt_ms', 'src': 'netma-test-3', 'dst': 'netma-test-1'},{'value': '1.445', 'metric': 'net_rtt_ms', 'src': 'netma-test-1', 'dst': 'netma-test-2'},{'value': '1.305', 'metric': 'net_rtt_ms', 'src': 'netma-test-1', 'dst': 'netma-test-3'}, {'value': '786006', 'metric': 'net_throughput_kbps', 'src': 'netma-test-2', 'dst': 'netma-test-3'}, {'value': '761528', 'metric': 'net_throughput_kbps', 'src': 'netma-test-2', 'dst': 'netma-test-1'}, {'value': '703681', 'metric': 'net_throughput_kbps', 'src': 'netma-test-3', 'dst': 'netma-test-2'}, {'value': '676144', 'metric': 'net_throughput_kbps', 'src': 'netma-test-3', 'dst': 'netma-test-1'}, {'value': '745310', 'metric': 'net_throughput_kbps', 'src': 'netma-test-1', 'dst': 'netma-test-2'}, {'value': '729834', 'metric': 'net_throughput_kbps', 'src': 'netma-test-1', 'dst': 'netma-test-3'}]
    nodos = {}
    links = []
    metrics = {}
    for valor in values:
        if valor['src'] not in nodos.keys():
            nodos[valor['src']]={"oNodeDegree":0, "oNodeThroughput":0}
        if valor['dst'] not in nodos:
            nodos[valor['dst']]={"oNodeDegree":0, "oNodeThroughput":0}
        if (valor['src'],valor['dst']) not in links:
            links.append((valor['src'],valor['dst']))
            nodos[valor['dst']]["oNodeDegree"] = nodos[valor['dst']]["oNodeDegree"]+0.5
            nodos[valor['src']]["oNodeDegree"] = nodos[valor['src']]["oNodeDegree"]+0.5
        if (valor['src'],valor['dst']) not in metrics.keys():
            metrics[(valor['src'],valor['dst'])]={}
            metrics[(valor['src'],valor['dst'])][valor["metric"]]=valor['value']
        else:
            metrics[(valor['src'],valor['dst'])][valor["metric"]]=valor['value']
        if valor["metric"] == 'net_throughput_kbps':
            nodos[valor['src']]["oNodeThroughput"]= nodos[valor['src']]["oNodeThroughput"] + int(valor['value'])/1e6
        elif valor["metric"] == 'net_rtt_ms':
            metrics[(valor['src'],valor['dst'])][valor["metric"]] = str(metrics[(valor['src'],valor['dst'])][valor["metric"]])+"e6"

    #print("NODOS:\t", nodos)
    #print("ENLACES:\t",links)
    #print("METRICAS:\t", metrics)

