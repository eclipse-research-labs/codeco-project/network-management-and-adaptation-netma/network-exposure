# Copyright (c) 2024 Telefónica Innovación Digital
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# SPDX-License-Identifier: Apache-2.0
# 
# Contributors:
#     Alejandro Muñiz - Autor
#     Luis M. Contreras - Coautor

FROM python:3.9-slim

RUN apt-get update && apt-get install -y libsnappy-dev && rm -rf /var/lib/apt/lists/*

COPY . /app/
WORKDIR /app
RUN pip3 install -r requirements.txt
CMD ["python3", "nemesys.py"]
# If you want to define any parameters please do it following the next flags:
# It is no needed to use them bothor to use in that order.
# CMD ["python3", "nemesys.py", "--delay", "60", "--probes", "netperf"]
